# Cen02
Given("I am logged in {string}") do |string|
  visit('/tasks')
end

When("I click on the link {string}") do |string|
  find_link('my_task').text
end

Then("I should see the message {string}") do |string|
  #expect(page).to have_content "Welcome, Rose Dias!"
end