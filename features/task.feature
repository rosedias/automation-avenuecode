#language: en
@login
Feature:  Create Task

    As a ToDo App user
    I should be able to create a task
    So I can manage my tasks

    Scenario: My Tasks link on the NavBar
        Given I am logged in "TodoAPP"
        When I click on the link "My Tasks"
        Then I should see the message "Rose Dias's ToDo List"   


 




