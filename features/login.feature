#language: en

Feature:  Login

    As a ToDo App user
    I should be able to log into the system
    So I can manage my tasks

    
    Scenario: Login
        Given that I'm in "todoapp"
        When I click on the "Sign in" line
        When I include the user 'rosedias.info@gmail.com' and the password '# Solzinha2020'
        When I click the "commit" button
        Then I am authenticated "Connected successfully."