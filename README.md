# Projeto Testes Automatizados Avenue Code #

Este projeto, foi desenvolvido para o cliente: Avenue Code. Neste projeto, existia a possibilidade de desenvolver, conforme seu conhecimento, habilidades e tecnologias de dominio,no caso um projeto de livre desenvolvimento conforme seu skill. Porém com alguns requisitos: 
* Automated UI Tests using BDD

# Técnicas de testes escolhida: 

  O projeto de teste apresenta as seguintes tecnologias:

- SO: Windows 10;
- Capybara opensource: Framework de Automação Teste;
- TDD: RSpec para testes de desenvolvimento;
- BDD: para testes de negócio;

# Estrutura do Projeto:

- automation-avenuecode: pasta principal do projeto;
- features: funcionalidades relacionadas ao projeto;
- spec: classes tdd para integrar com BDD
- cucumber: 


Scenario: The user should see a message on the top part saying that list belongs to the logged user.
    Pre condition: If the logged user name is John, then the displayed message should be ‘Hey John, this is your todo list for today:''
        Given that I'm in "ToDO APP" 
        When click on "addTask" on the NavBar
        Then should i view the message "Rose Dias's ToDo List"

    Scenario: The user should be able to enter a new task by hitting enter or clicking on the add task button.
        Given that I'm in "ToDo App My Tasks"
        When fill in the field "addTask" with "Task01"
        When click on "addTask"
        Then should I view in the "To be Done" Task01

    Scenario: The task should require at least three characters so the user can enter it.
        Given that I'm in "ToDo App My Tasks"
        When click the button "addTask"
        Then should i view the message "The task should require at least three characters"

    Scenario: The task can’t have more than 250 characters.
        Given hat I'm in "ToDo App My Tasks"
        When fill in the field "addTask" with "250 characters"
        Then should i view the message "The task can’t have more than 250 characters"

    Scenario: When added, the task should be appended on the list of created tasks.
        Given hat I'm in "ToDo App My Tasks"
        When fill in the field "addTask" with "Task01"
        When cclick the button "addTask"
        Then should I view in the list "To be Done" Task01